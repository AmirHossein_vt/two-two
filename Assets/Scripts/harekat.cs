﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class harekat : MonoBehaviour {
	private float middlepoint;
    public Rigidbody2D LP, RP, ball , ball1;
	private Vector3 offset;
    public float speed = 0;
    private bool accident = true ;
    Vector3 playerPosScreen;
    Vector3 wrld;
    float half_szX;
    float half_szY;


    void Start () {
        middlepoint = Screen.width / 2;
        wrld = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));
        half_szX = GetComponent<Renderer>().bounds.size.x / 2;
        half_szY = GetComponent<Renderer>().bounds.size.y / 2;
    }

	void Update () {
 
        CheckMousePress();
        if (accident == true)
        {
            Vector3 v = Quaternion.AngleAxis(Random.Range(-90.0f, 90.0f), Vector3.forward) * Vector3.up;
            ball.velocity = v * speed;
            Vector3 v1 = Quaternion.AngleAxis(Random.Range(90.0f, 270.0f), Vector3.forward) * Vector3.up;
            ball1.velocity = v1 * speed;
            accident = false;
        }
        playerPosScreen = ball.position;
        if ( playerPosScreen.x >= (wrld.x - half_szX))
        {
            playerPosScreen.x = wrld.x - half_szX;
            ball.position = playerPosScreen;
        }
        if (playerPosScreen.x <= -(wrld.x - half_szX))
        {
            playerPosScreen.x = -(wrld.x - half_szX);
            ball.position = playerPosScreen;
        }
        playerPosScreen = ball1.position;
        if (playerPosScreen.x >= (wrld.x - half_szX))
        {
            playerPosScreen.x = wrld.x - half_szX;
            ball1.position = playerPosScreen;
        }
        if (playerPosScreen.x <= -(wrld.x - half_szX))
        {
            playerPosScreen.x = -(wrld.x - half_szX);
            ball1.position = playerPosScreen;
        }


    }



    private void CheckMousePress()
    {
     

        if (Input.GetMouseButton(0)) { 
             if (Input.mousePosition.x < middlepoint) LeftSideHandler();
            if (Input.mousePosition.x > middlepoint) RightSideHandler();
                }
	}


    private void LeftSideHandler()
    {

        LP.transform.position = Camera.main.ScreenToWorldPoint(new Vector3((Screen.width / 40) , Input.mousePosition.y, 10.0f));


    }
    private void RightSideHandler() 
    {
            RP.transform.position = Camera.main.ScreenToWorldPoint(new Vector3( (Screen.width - (Screen.width / 30)) , Input.mousePosition.y, 10.0f));

    }




}
